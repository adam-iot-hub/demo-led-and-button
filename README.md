# ESP8266 Demo LED/Button MQTT

Showing off some ESP8266 functionality.

Slides are [here](https://docs.google.com/presentation/d/1mphITtd69_mYW7rQNuvDZ_pq7NQHtq9dMxP4rq1Aa_0/)

## Quick Start

Make a circuit with a button and an LED connected to GPIO ([example](http://www.multiwingspan.co.uk/arduino.php?page=led4)).

Set `LED_PIN` and `BUTTON_PIN` in `src/main.cpp`.

Copy `src/config.h.example` to `src/config.h` and set `ssid` and `password` for your wifi.

Install [platformio](https://docs.platformio.org/en/latest/installation.html)

Run `pio run -t upload` to compile and flash your microcontroller. `pio device monitor` opens your serial monitor.

Finally, I use [mosquitto](https://mosquitto.org) as my MQTT broker and [Node-RED](https://nodered.org/) for programming IoT flows.
