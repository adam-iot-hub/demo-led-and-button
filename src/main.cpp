#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include <PubSubClient.h>

#include "config.h"

WiFiClient wifi_client;

IPAddress MQTTserver(192, 168, 1, 86);

PubSubClient client(wifi_client);

#ifndef MQTT_ID
#define MQTT_ID "DEMO_LED1"
#endif

#define LED_PIN 2
#define BUTTON_PIN 0

int lastButtonState = 0;
char *buf;

void mqtt_callback(char *topic, byte *payload, unsigned int length)
{
    payload[length] = '\0';
    String s = String((char *)payload);
    String t = String(topic);

    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    Serial.println(s);

    if (t == MQTT_ID "/set")
    {
        int led_brightness = s.toInt();
        Serial.println("Setting brighness to " + String(led_brightness));
        analogWrite(LED_PIN, led_brightness);
    }
}

void mqtt_connect()
{
    while (!client.connected())
    {
        Serial.print("Attempting MQTT connection...");

        String clientId = MQTT_ID;
        // Attempt to connect
        if (client.connect(clientId.c_str()))
        {
            Serial.println("connected sending status/ hello world");
            // Once connected, publish an announcement...
            Serial.println(client.publish(MQTT_ID "/status", "hello world"));
            // ... and resubscribe
            Serial.println(client.subscribe(MQTT_ID "/set"));
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void setup()
{
    Serial.begin(115200);
    Serial.println("booting\nconnect to wifi");

    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    ArduinoOTA.setHostname(MQTT_ID);
    ArduinoOTA.begin();

    Serial.print("hostname: ");
    Serial.println(MQTT_ID);

    Serial.println("setting LED to 0");
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, HIGH);

    pinMode(BUTTON_PIN, INPUT);

    Serial.print(WiFi.localIP());
    Serial.print(" on ");
    Serial.println(ssid);

    client.setServer(MQTTserver, 1883);
    client.setCallback(mqtt_callback);
}

void loop()
{
    ArduinoOTA.handle();

    mqtt_connect();
    client.loop();

    int button_reading = digitalRead(BUTTON_PIN);
    if (button_reading != lastButtonState)
    {
        lastButtonState = button_reading;
        if (button_reading)
        {
            buf = "1";
        }
        else
        {
            buf = "0";
        }

        Serial.print("sending ");
        Serial.print(buf);
        Serial.print(" to /button (");
        Serial.print(client.publish(MQTT_ID "/button", buf));
        Serial.println(")");
    }
}
